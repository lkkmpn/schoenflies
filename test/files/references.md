# References

All geometries in this directory were sourced from:

NIST Computational Chemistry Comparison and Benchmark Database,
NIST Standard Reference Database Number 101
Release 21, August 2020, Editor: Russell D. Johnson III
http://cccbdb.nist.gov/
