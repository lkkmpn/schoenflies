/**
 * Schoenflies
 * Copyright (c) 2022 Luuk Kempen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GUI_LIBRARY_ITEM_DELEGATE_H
#define GUI_LIBRARY_ITEM_DELEGATE_H

#include <Qt>
#include "item_delegate.h"

class LibraryItemDelegate: public ItemDelegate {
    Q_OBJECT

public:
    enum ItemDataRole {
        PathRole = Qt::ItemDataRole::UserRole + 1
    };
};

#endif  // GUI_LIBRARY_ITEM_DELEGATE_H
